import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://3.19.73.74:5000',
});

export default instance;
