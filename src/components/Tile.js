import { Link } from 'react-router-dom';

function Tile({ label, image, to }) {
    return (
        <Link to={to} style={{ textDecoration: 'none', padding:"10px"}}>
            <div style={styles.tile}>
                <img src={image} style={styles.image} alt={label} />
                <div style={styles.label}>{label}</div>
            </div>
        </Link>
    );
}

const styles = {
    tile: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        width: '200px',
        height: '200px',
        borderRadius: '10px',
        backgroundColor: '#333333',
        boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.2)',
        transition: 'box-shadow 0.2s ease-in-out',
        cursor: 'pointer',
        marginBottom: '20px',
        padding: '10px',
    },
    image: {
        width: '100%',
        height: '100%',
        objectFit: 'cover',
        borderRadius: '10px',
    },
    label: {
        fontSize: '20px',
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: '10px',
        color: '#f7d51d',
    },
};

export default Tile;