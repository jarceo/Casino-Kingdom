import { FaBars } from "react-icons/fa";
import { NavLink as Link } from "react-router-dom";
import styled from "styled-components";

export const Nav = styled.nav`
background: #333333;
height: 85px;
display: flex;
justify-content: space-between;
padding: 0.2rem;
padding-right: 5em;
z-index: 12;
`;

export const NavLink = styled(Link)`
color: #808080;
display: flex;
position: absolute;
top: 30px;
left: 25px;
text-decoration: none;
padding: 0 1rem;
height: 100%;
cursor: pointer;
&.active {
	color: #000000;
}
`;




export const Bars = styled(FaBars)`
display: none;
color: #808080;
@media screen and (max-width: 768px) {
	display: block;
	position: absolute;
	top: 0;
	right: 0;
	transform: translate(-100%, 75%);
	font-size: 1.0rem;
	cursor: pointer;
}
`;

export const NavMenu = styled.div`
display: flex;
margin-left: auto;
align-items:center;
column-gap: 2em;
/* Second Nav */
/* margin-right: 24px; */
/* Third Nav */
/* width: 100vw;
white-space: nowrap; */
@media screen and (max-width: 768px) {
	display: none;
}
`;

export const NavText = styled("div")`
  color: #f7d51d;
  display: flex;
  align-items: center;
  border: none;
  background: none;
  font-size: 1.0rem;
  text-decoration: none;
  cursor: default;
  font-family: "Luckiest Guy", cursive;
  
  
`;

export const NavBtn = styled("button")`
  color: #f7d51d;
  display: flex;
  align-items: center;
  border: none;
  background: none;
  font-size: 1.0rem;
  font-family: "Luckiest Guy", cursive;
  text-decoration: none;
  cursor: pointer;

  &.active {
    color: #000000;
  }

`;