import React from "react";
import {Nav, NavBtn, NavLink, NavMenu, NavText}
    from "./NavbarElements";

class Navbar extends React.Component {

    static hideElements() {
        document.getElementById("navElems").style.display = "none";
    }
    static logout() {
        console.log("Logging out...");
        localStorage.username = '';
        localStorage.uid = '';
        localStorage.isLoggedIn = false;
        window.location.href = "/";
    }
    render() {
        return (

        <div id = {"navElems"}>
            <Nav>
                <NavMenu>
                    <NavLink to="/" >Home</NavLink>
                    {/*<NavLink to="/CoinFlip" >*/}
                    {/*    CoinFlip*/}
                    {/*</NavLink>*/}
                    {/*<NavLink to="/contact" >*/}
                    {/*    Contact Us*/}
                    {/*</NavLink>*/}
                    {/*<NavLink to="/blogs" >*/}
                    {/*    Blogs*/}
                    {/*</NavLink>*/}
                    <NavText>Logged in as {localStorage.username}</NavText>
                    <NavText>Balance: {localStorage.balance}</NavText>
                    <NavBtn onClick = {() => {Navbar.logout()}}>Logout</NavBtn>
                </NavMenu>
            </Nav>
        </div>
        );
    }
}

export default Navbar;
