import './App.css';
import Navbar from './components/Navbar/Navbar';
import {BrowserRouter as Router, Routes, Route}
    from 'react-router-dom';
import CoinFlip from "./pages/CoinFlip";
import Home from "./pages/Home";
import Register from "./pages/Register";
import Login from "./pages/Login";
import React from "react";
import axios from "./API/axios";


class App extends React.Component{

    async componentDidMount() {
        console.log("Checking if user is logged in...");
        try {
            const location = window.location.href;
            const sub = location.substring(location.lastIndexOf("/"), location.length);
            console.log(sub);
            if(sub.includes("/Login") || sub.includes("/Register")) {
                Navbar.hideElements();
                return;
            }
            if(!localStorage.uid || !localStorage.username || !localStorage.isLoggedIn) {
                console.log("User is not logged in");
                Navbar.hideElements();
                localStorage.isLoggedIn = false;
                localStorage.username = '';
                localStorage.uid = '';
                window.location.href = "/Login";
                return;
            }
            const response = await axios.post("/isLoggedIn", {uid: localStorage.uid, username: localStorage.username});
            console.log(response.data);
            if(response.data) {
                localStorage.isLoggedIn = true;
            } else {
                localStorage.username = '';
                localStorage.uid = '';
                localStorage.isLoggedIn = false;
            }
        } catch (error) {
            console.error(error);
        }

    }


    render() {
        console.log("Rendering App.js");
        return (
            <Router>
                <Navbar/>
                <Routes>
                    <Route path='/' element={<Home/>}/>
                    <Route path='/CoinFlip' element={<CoinFlip/>}/>
                    <Route path='/Home' element={<Home/>}/>
                    <Route path='/Register' element={<Register/>}/>
                    <Route path='/Login' element={<Login/>}/>
                </Routes>
            </Router>
        );
    }
}

export default App;
