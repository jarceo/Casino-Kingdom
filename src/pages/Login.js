import React, { useState } from "react";
import axios from "../API/axios";
import background from "../images/casino_bg.jpg";

const Login = () => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState("");

    const clearLocalStorage = () => {
        localStorage.username = '';
        localStorage.isLoggedIn = false;
        localStorage.uid = '';
        localStorage.firstname = '';
        localStorage.lastname = '';
        localStorage.balance = '';
    }

    const setLocalStorage = (username, uid, firstname, lastname, balance) => {
        localStorage.username = username;
        localStorage.uid = uid;
        localStorage.firstname = firstname;
        localStorage.lastname = lastname;
        localStorage.balance = balance;
        localStorage.isLoggedIn = true;
    }
    const handleUsernameChange = (event) => {
        setUsername(event.target.value);
    };

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (username === "" || password === "") {
            setError("Please fill out all fields");
            return;
        }
        try {
            const response = await axios.post("/login", {
                username: username,
                password: password,
            });

            if(response.data){
                setLocalStorage(username, response.data[0].uid, response.data[0].firstname, response.data[0].lastname, response.data[0].balance)
                setError("");
                window.location.href = "/";
            }else{
                clearLocalStorage();
                setError("Invalid username or password");
            }
            console.log(response.data);

        } catch (error) {
            console.error(error);
            clearLocalStorage();
            setError("Invalid username or password");
        }
    };

    return (
        <div style={styles.container}>
            <form onSubmit={handleSubmit} style={styles.form}>
                <h1 style={styles.title}>Casino Login</h1>
                <div style={styles.formGroup}>
                    <label htmlFor="username" style={styles.label}>
                        Username
                    </label>
                    <input
                        type="username"
                        id="username"
                        value={username}
                        onChange={handleUsernameChange}
                        style={styles.input}
                    />
                </div>
                <div style={styles.formGroup}>
                    <label htmlFor="password" style={styles.label}>
                        Password
                    </label>
                    <input
                        type="password"
                        id="password"
                        value={password}
                        onChange={handlePasswordChange}
                        style={styles.input}
                    />
                </div>
                <div style={styles.registerMessage}>Don't have an account?<span> </span>
                    <a style = {styles.linkButton}href={"/Register"}>Register here</a>
                </div>
                {error && <div style={styles.errorMessage}>{error}</div>}
                <button type="submit" style={styles.button}>
                    Login
                </button>
            </form>
        </div>
    );
};

const styles = {
    container: {
        display: "flex",
        backgroundImage: `url(${background})`,
        backgroundSize: "cover",
        backgroundBlendMode: "luminosity",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
        backgroundColor: "#1d1d1d",
    },
    form: {
        backgroundColor: "#333333",
        padding: "50px",
        borderRadius: "10px",
        boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.2)",
    },
    title: {
        marginBottom: "20px",
        fontFamily: "'Luckiest Guy', cursive",
        fontSize: "50px",
        color: "#f7d51d",
        textShadow: "2px 2px #000000",
    },
    formGroup: {
        marginBottom: "20px",
    },
    label: {
        display: "block",
        marginBottom: "5px",
        fontFamily: "'Montserrat', sans-serif",
        fontSize: "20px",
        color: "#f7d51d",
        textShadow: "1px 1px #000000",
    },
    input: {
        width: "100%",
        padding: "10px",
        fontSize: "16px",
        border: "none",
        borderRadius: "5px",
        backgroundColor: "#1d1d1d",
        color: "#ffffff",
        fontFamily: "'Montserrat', sans-serif",
    },
    button: {
        display: "block",
        marginTop: "20px",
        padding: "10px 20px",
        fontSize: "16px",
        border: "none",
        borderRadius: "5px",
        backgroundColor: "#f7d51d",
        color: "#000",
        fontFamily: "'Montserrat', sans-serif",
        cursor: "pointer",
        transition: "background-color 0.2s ease-in-out",
        boxShadow: "2px 2px #000000",
    },
    buttonHover: {
    backgroundColor: "#ffd500",
},
errorMessage: {
        color: "red",
        marginBottom: "10px",
        fontFamily: "'Montserrat', sans-serif",
        fontSize: "16px",
},
registerMessage: {
        color: "#f7d51d",
        marginBottom: "10px",
        fontFamily: "'Montserrat', sans-serif",
        fontSize: "16px",
    },
    linkButton: {
        color: "#ffd500",
    }
};

export default Login;