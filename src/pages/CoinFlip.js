import React, {Component, useState} from 'react'
import Coin from './Coin'
import axios from "../API/axios";
import Navbar from "../components/Navbar/Navbar";


class CoinFlip extends Component {


    flipCoin() {
        const coins = [
            {side: 'heads', imgSrc: 'https://media.geeksforgeeks.org/wp-content/uploads/20200916123059/SHalfDollarObverse2016head-300x300.jpg'},
            {side: 'tails', imgSrc: 'https://media.geeksforgeeks.org/wp-content/uploads/20200916123125/tails-200x200.jpg'}
        ]
        const randomIdx = Math.floor(Math.random() * coins.length)
        return coins[randomIdx]
    }



    handleClick = async (e) => {
        // e.preventDefault();
        const bet = parseInt(document.getElementById("bet").value) || 0;
        const errorMessage = document.getElementById("errorMessage");
        console.log (bet);
        if(bet <= 0||bet>localStorage.balance){
            console.log("Invalid bet amount");
            errorMessage.innerText = "Invalid bet amount";
            return;
        }
        errorMessage.innerText = "";
        if(e.target.innerText == "Heads"){
            console.log("Heads");
        }else {
            console.log("Tails");
        }

        try {
            this.setState({bet: document.getElementById("bet").value});
            const currBal = await axios.post("/get-balance", {
                username: localStorage.username,
                uid: localStorage.uid
            });
            console.log(currBal);
            localStorage.balance = currBal.data[0].balance;
            if(bet>localStorage.balance){
                console.log("Invalid bet amount");
                errorMessage.innerText = "Invalid bet amount";
                return;
            }
            const response = await axios.post("/update-balance", {
                username: localStorage.username,
                uid: localStorage.uid,
                changeBal: bet*-1,
                currBal: localStorage.balance
            });
            console.log(response);
            localStorage.balance = response.data[0].balance;
        } catch (error) {
            console.log(error);
            throw error;
        }
        const coin = this.flipCoin();
        document.getElementById("coinImage").src = coin.imgSrc;
        if(coin.side == e.target.innerText.toLowerCase()){
            console.log("You win");
            const response = await axios.post("/update-balance", {
                username: localStorage.username,
                uid: localStorage.uid,
                changeBal: bet*2,
                currBal: localStorage.balance
            });
            console.log(response);
            document.getElementById("errorMessage").innerText = "It was " + coin.side + " You win!";
            document.getElementById("errorMessage").style.color = "green";

            localStorage.balance = response.data[0].balance;
        }else {
            console.log("You lose");
            document.getElementById("errorMessage").innerText = "It was " + coin.side + "You lose!";
            document.getElementById("errorMessage").style.color = "red";
        }
    }
    render(){
        return(
            <div style={styles.container}>
                <img style={{ width:'200px', height:'200px' }} id={"coinImage"} src={'https://media.geeksforgeeks.org/wp-content/uploads/20200916123059/SHalfDollarObverse2016head-300x300.jpg'}/>
                <h2 style={styles.welcome}>Let's flip a coin</h2>

                {/* If current face exist then show current face */}
                {/*{currFace && <Coin info={currFace} />}*/}

                {/* Button to flip the coin */}
                <div style={styles.formGroup}>
                    <label style={styles.label}>
                        Bet amount
                    </label>
                    <label style={styles.errorMessage} id={"errorMessage"}>
                    </label>
                    <input
                        type="text"
                        id="bet"

                        style={styles.input}
                    />
                </div>
                <button style={styles.button} onClick={this.handleClick}>Heads</button>
                <button style={styles.button} onClick={this.handleClick}>Tails</button>

            </div>
        );
    }
}

const styles = {
    container: {
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: "#1d1d1d",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
    },
    welcome: {
        fontFamily: "'Luckiest Guy', cursive",
        color: "#f7d51d",
    },
    button: {
        display: "block",
        marginTop: "20px",
        padding: "10px 20px",
        fontSize: "16px",
        border: "none",
        borderRadius: "5px",
        backgroundColor: "#f7d51d",
        color: "#000",
        fontFamily: "'Montserrat', sans-serif",
        cursor: "pointer",
        transition: "background-color 0.2s ease-in-out",
        boxShadow: "2px 2px #000000",
    },
    buttonHover: {
        backgroundColor: "#ffd500",
    },
    input: {
        width: "100%",
        padding: "10px",
        fontSize: "16px",
        border: "none",
        borderRadius: "5px",
        backgroundColor: "#333333",
        color: "#ffffff",
        fontFamily: "'Montserrat', sans-serif",
    },
    label: {
        display: "block",
        marginBottom: "5px",
        fontFamily: "'Montserrat', sans-serif",
        fontSize: "20px",
        color: "#f7d51d",
        textShadow: "1px 1px #000000",
    },
    errorMessage: {
        color: "red",
        marginBottom: "10px",
        fontFamily: "'Montserrat', sans-serif",
        fontSize: "16px",
    },
}

export default CoinFlip;
