import React, { useState } from "react";
import axios from "../API/axios";
import background from "../images/casino_bg.jpg";




//	uid int
//	username varchar(255)
//	password varchar(255)
//	balance int
//	firstname varchar(255)
//	lastname varchar(255)
const Register = () => {
    var uid;
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const[firstname, setFirstname] = useState("");
    const[lastname, setLastname] = useState("");

    const clearLocalStorage = () => {
        localStorage.username = '';
        localStorage.isLoggedIn = false;
        localStorage.uid = '';
        localStorage.firstname = '';
        localStorage.lastname = '';
        localStorage.balance = '';
    }

    const setLocalStorage = (username, uid, firstname, lastname, balance) => {
        localStorage.username = username;
        localStorage.uid = uid;
        localStorage.firstname = firstname;
        localStorage.lastname = lastname;
        localStorage.balance = balance;
        localStorage.isLoggedIn = true;
    }

    const handleRegister = async (e) => {
        e.preventDefault();
        if(username === "" || password === "" || confirmPassword === "" || firstname === "" || lastname === ""){
            document.getElementById("bottomOutputLbl").textContent = "Please fill out all fields";
            clearLocalStorage();
            return;
        }
        if (password !== confirmPassword) {
            document.getElementById("bottomOutputLbl").textContent = "Passwords do not match";
            clearLocalStorage();

            return;
        }
        try {
            const response = await axios.get("/get-users")
            console.log(response.data)

            //checks if username exists
            for(var i = 0; i<response.data.length; i++){
                if(username === response.data[i].username){
                    document.getElementById("bottomOutputLbl").textContent = "Username already exists pick a new one";
                    clearLocalStorage();
                    return;
                }
            }
            uid = Math.floor(Math.random() * 1000000000);
            console.log(uid);
            //Checks to make sure UID is unique
            var unique = false;
            while(!unique){
                for(var i = 0; i<response.data.length; i++){
                    if(uid == response.data[i].uid){
                        uid = Math.floor(Math.random() * 1000000000);
                        console.log(uid);
                        break;
                    }
                    if(i == response.data.length-1){
                        unique = true;
                    }
                }
            }
        }catch (error){
            document.getElementById("topOutputLbl").style.display="block";
            document.getElementById("topOutputLbl").textContent = "Problem registering user, try again at a different time"
            clearLocalStorage();
            throw error;

        }

        try {
            const response = await axios.post("/create-user", {
                uid: uid,
                username,
                password,
                balance: 100,
                firstname,
                lastname
            });
            setLocalStorage(username, uid, firstname, lastname, 100);
            console.log(response.data);
            console.log("Username: " + localStorage.username);
            document.getElementById("topOutputLbl").style.display="block";
            document.getElementById("bottomOutputLbl").style.display="none";
            for(var i = 0; i < document.getElementsByClassName("inputElements").length;i++) {
                document.getElementsByClassName("inputElements")[i].style.visibility = 'hidden';
            }
        } catch (error) {
            clearLocalStorage();
            document.getElementById("topOutputLbl").textContent = "Error registering user";
        }
    };

    return (
        <div
            style={styles.container}
        >
            <form onSubmit={handleRegister} style={styles.form}>
            <h1 style={styles.title}>Create an account</h1>

            <label
                style={{
                    marginBottom: "1rem",
                    display: "none",
                    backgroundColor: "#b3ffb3",
                    color: "#333",
                    padding: "0.5rem",
                    borderRadius: "10px",
                }}
                id={"topOutputLbl"}
            >
                <h2 style={{ textAlign: "center" }}>Registered successfully!</h2>
                Click <a href={"/"}>here</a> if you are not automatically redirected
            </label>

            <label className={"inputElements"} style={styles.label}>
                First Name
                <input
                    type="text"
                    value={firstname}
                    onChange={(e) => setFirstname(e.target.value)}
                    style={styles.input}

                />
            </label>
            <br />
            <label className={"inputElements"} style={styles.label}>
                Last Name
                <input
                    type="text"
                    value={lastname}
                    onChange={(e) => setLastname(e.target.value)}
                    style={styles.input}

                />
            </label>
            <br />
            <label className={"inputElements"} style={styles.label}>
                Username
                <input
                    type="text"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                    style={styles.input}

                />
            </label>
            <br />
            <label className={"inputElements"} style={styles.label}>
                Password
                <input
                    type="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    style={styles.input}

                />
            </label>
            <br />
            <label className={"inputElements"} style={styles.label}>
                Confirm Password
                <input
                    type="password"
                    value={confirmPassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}
                    style={styles.input}
                />
            </label>
                <div style={styles.registerMessage}>Already have an account?<span> </span>
                    <a style = {styles.linkButton}href={"/Login"}>Login here</a>
                </div>
            <br />
            <button
                className={"inputElements"}
                onClick={handleRegister}
                style={styles.button}
            >
                Register
            </button>
                    <label
                style={styles.errorMessage}
                id="bottomOutputLbl"
            >

            </label>
            </form>
        </div>
    );
};

const styles = {
    container: {
        backgroundImage: `url(${background})`,
        backgroundSize: "cover",
        backgroundBlendMode: "luminosity",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
        backgroundColor: "#1d1d1d",
    },
    title: {
        marginBottom: "20px",
        fontFamily: "'Luckiest Guy', cursive",
        fontSize: "50px",
        color: "#f7d51d",
        textShadow: "2px 2px #000000",
    },
    form: {
        backgroundColor: "#333333",
        padding: "50px",
        borderRadius: "10px",
        boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.2)",
    },
    formGroup: {
        marginBottom: "20px",
    },
    label: {
        display: "block",
        marginBottom: "5px",
        fontFamily: "'Montserrat', sans-serif",
        fontSize: "20px",
        color: "#f7d51d",
        textShadow: "1px 1px #000000",
    },
    input: {
        width: "100%",
        padding: "10px",
        fontSize: "16px",
        border: "none",
        borderRadius: "5px",
        backgroundColor: "#1d1d1d",
        color: "#ffffff",
        fontFamily: "'Montserrat', sans-serif",
    },
    button: {
        display: "block",
        marginTop: "20px",
        padding: "10px 20px",
        fontSize: "16px",
        border: "none",
        borderRadius: "5px",
        backgroundColor: "#f7d51d",
        color: "#000",
        fontFamily: "'Montserrat', sans-serif",
        cursor: "pointer",
        transition: "background-color 0.2s ease-in-out",
        boxShadow: "2px 2px #000000",
    },
    buttonHover: {
        backgroundColor: "#ffd500",
    },
    errorMessage: {
        color: "red",
        marginBottom: "10px",
        fontFamily: "'Montserrat', sans-serif",
        fontSize: "16px",
    },
    registerMessage: {
        color: "#f7d51d",
        marginBottom: "10px",
        fontFamily: "'Montserrat', sans-serif",
        fontSize: "16px",
    },
    linkButton: {
        color: "#ffd500",
    }

}

export default Register;
