import React from "react";
import Tile from "../components/Tile";
import coinflip from "../images/coinflip.jpg";
import indev from "../images/wip.jpg";


class Home extends React.Component{
    render(){
    return (
        <div style={{backgroundColor: "#1d1d1d",height:"100vh"}}>
            <div style={styles.container}>
            <h1 style={styles.welcome}>Welcome back {localStorage.firstname}! <br/> Available Games:</h1>
                </div>
        <div>
            <div style={styles.container}>
                <Tile
                    label="Slot Machine"
                    image= {indev}
                    to="/"
                />
                <Tile
                    label="Coin Flip"
                    image= {coinflip}
                    to="/CoinFlip"
                />
                <Tile
                    label="Black Jack"
                    image= {indev}
                    to="/"
                />
            </div>
        </div>
        </div>

    );
    }
}

const styles = {
    container: {
        display: "flex",
        backgroundColor: "#1d1d1d",
        justifyContent: "center",
        alignItems: "center",
    },
    welcome: {
        fontFamily: "'Luckiest Guy', cursive",
        color: "#f7d51d",
    }
}

export default Home;
